﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TextRPG {

    public class Encounter : MonoBehaviour {

        public Enemy Enemy { get; set; }

        public delegate void OnEnemyDieHandler();
        public static OnEnemyDieHandler OnEnemyDie;

        [SerializeField] Player Player;
        [SerializeField] Button[] DynamicControls;

        public void Start() {

            OnEnemyDie += Loot;
        }

        public void Awake() {
            ResetDynamicControls();
        }

        public void ResetDynamicControls() {

            foreach(Button button in DynamicControls ) {

                button.interactable = false;
            }
        }

        public void StartCombat() {

            this.Enemy = Player.Room.Enemy;
            DynamicControls[0].interactable = true;
            DynamicControls[1].interactable = true;
            UIController.OnEnemyUpdate( this.Enemy );
        }

        public void StartChest() {

            DynamicControls[3].interactable = true;
        }

        public void StartExit() {

            DynamicControls[2].interactable = true;
        }

        public void OpenChest() {

            Chest Chest = Player.Room.Chest;
            if( Chest.Trap ) {
                Player.TakeDamage( 5 );
                Journal.Instance.Log( "It was a trap! you took 5 damage." );
            }else if( Chest.Heal ) {
                Player.TakeDamage( -7 );
                Journal.Instance.Log( "It contained a healing spell! you gained 7 energy." );
            }else if( Chest.Enemy ) {
                Player.Room.Enemy = Chest.Enemy;
                Player.Room.Chest = null;
                Journal.Instance.Log( "The chest contained a monster. Oh no!" );
                Player.Investigate();
            } else {
                Player.Gold += Chest.Gold;
                Player.AddItem( Chest.Item );
                UIController.OnPlayerStatChange();
                UIController.OnPlayerInventoryChange();
                Journal.Instance.Log( "You found: " + Chest.Item + " and <color=#FFE556FF>" + Chest.Gold + "g</color>." );
            }
            Player.Room.Chest = null;
            DynamicControls[3].interactable = false;
        }

        public void Attack() {

            int PlayerDamageAmount = (int)(Random.value * ( Player.Attack - Enemy.Defence ));
            int EnemyDamageAmount = (int) (Random.value * ( Enemy.Attack - Player.Defence ));

            Player.TakeDamage( EnemyDamageAmount );
            Enemy.TakeDamage( PlayerDamageAmount );

            Journal.Instance.Log( "<color=#59ffa1ff>You attacked, dealing <b>" + PlayerDamageAmount + "</b> damage!</color>" );
            Journal.Instance.Log( "<color=#59ffa1ff>The enemy retaliated dealing <b>" + EnemyDamageAmount + "</b> damage!</color>" );
        }

        public void Flee() {

            int EnemyDamageAmount = (int) ( Random.value * ( Enemy.Attack - (Player.Defence *.5f) ) );
            Player.Room.Enemy = null;
            UIController.OnEnemyUpdate( null );
            Player.TakeDamage( EnemyDamageAmount );
            Journal.Instance.Log( "<color=#59ffa1ff>You fled the fight, taking <b>" + EnemyDamageAmount + "</b> damage.</color>" );
            Player.Investigate();
        }

        public void ExitFloor() {

            StartCoroutine( Player.World.GenerateFloor());
            Player.Floor++;
            Journal.Instance.Log( "You found an exit to another floor. Floor: " + Player.Floor );
        }

        public void Loot() {

            Player.AddItem( this.Enemy.Inventory[0] );
            Player.Gold += this.Enemy.Gold;
            Journal.Instance.Log(string.Format(
                "<color=#56ffc7ff>You've slain {0}. Searching the carcass, you find a {1},and {2} gold!</color>",
                this.Enemy.Description,
                this.Enemy.Inventory[0],
                this.Enemy.Gold
            ));
            Player.Room.Enemy = null;
            Player.Room.Empty = true;
            UIController.OnEnemyUpdate(null);
            UIController.OnPlayerInventoryChange();
            UIController.OnPlayerStatChange();
            Player.Investigate();
        }
    }
}
