﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TextRPG {

    public class Chest {

        public string Item { get; set; }
        public int Gold { get; set; }
        public bool Trap { get; set; }
        public bool Heal { get; set; }
        public Enemy Enemy { get; set; }

        public Chest() {

            switch( Random.Range( 0, 7 ) ) {

                case 0:
                case 1:
                    Heal = true;
                    break;
                case 2:
                    Trap = true;
                    break;
                case 3:
                    Enemy = EnemyDatabase.Instance.GetRandomEnemy();
                    break;
                default:
                    int itemToAdd = Random.Range( 0, ItemDatabase.Instance.Items.Count );
                    Item = ItemDatabase.Instance.Items[itemToAdd];
                    Gold = Random.Range( 20, 200 );
                    break;
            }
        }
    }
}