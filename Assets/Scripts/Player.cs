﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TextRPG {

    public class Player : Character
    {

        public int Floor { get; set; }
        public Room Room { get; set; }

        public World World;

        [SerializeField] Encounter Encounter;

        private void Start() {

            Floor = 0;
            Energy = 30;
            Attack = 10;
            Defence = 5;
            Gold = 0;
            //Inventory = new List<string>(); // I don't think this is necessary, already initialised in Character
            RoomIndex = new Vector2( 2, 2 );
            this.Room = World.Dungeon[(int) RoomIndex.x, (int) RoomIndex.y];
            this.Room.Empty = true;
            UIController.OnPlayerStatChange();
            UIController.OnPlayerInventoryChange();
        }

        public void Move(int direction ) {

            if( this.Room.Enemy ) {
                return;
            }

            // North
            if( direction == 0 && RoomIndex.y > 0 ) {
                Journal.Instance.Log( "You head North..." );
                RoomIndex -= Vector2.up;
            }

            // East
            if(direction == 1 && RoomIndex.x < World.Dungeon.GetLength(0) -1) {
                Journal.Instance.Log( "You head East..." );
                RoomIndex += Vector2.right;
            }

            // South
            if(direction == 2 && RoomIndex.y < World.Dungeon.GetLength(1) -1) {
                Journal.Instance.Log( "You head South..." );
                RoomIndex -= Vector2.down;
            }

            // West
            if(direction == 3 && RoomIndex.x > 0) {
                Journal.Instance.Log( "You head West..." );
                RoomIndex += Vector2.left;
            }
            if(this.Room.RoomIndex != RoomIndex ) {
                Investigate();
            }
        }

        public void Investigate() {

            this.Room = World.Dungeon[(int) RoomIndex.x, (int) RoomIndex.y];
            Encounter.ResetDynamicControls();

            if( this.Room.Empty ) {

                Journal.Instance.Log( "You find yourself in an empty room." );

            }else if(this.Room.Chest != null ) {

                Encounter.StartChest();
                Journal.Instance.Log( "You have found a chest! what would you like to do?" );

            }else if (this.Room.Enemy != null) {

                Encounter.StartCombat();
                Journal.Instance.Log( "You have stumbled across a " + Room.Enemy.Description + "!\nWhat would you like to do?" );

            }else if( this.Room.Exit ) {

                Encounter.StartExit();
                Journal.Instance.Log( "You've found the exit to the next floor.\nWould you like to continue?" );

            }
        }

        public void AddItem(string item) {

            Journal.Instance.Log( "You were given item: " + item );
            Inventory.Add( item );
            UIController.OnPlayerInventoryChange();
        }

        public void AddItem(int item) {

            Inventory.Add( ItemDatabase.Instance.Items[item] );
            UIController.OnPlayerInventoryChange();
        }

        public override void TakeDamage( int amount ) {

            Debug.Log( "Player has taken damage!" );
            base.TakeDamage( amount );
            UIController.OnPlayerStatChange();
        }

        public override void Die() {

            Debug.Log( "Player died. Game Over!" );
            base.Die();
        }

    }
}
