﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TextRPG {

    public class Walrus : Enemy
    {

        private void Start() {
            Energy = 15;
            MaxEnergy = 15;
            Attack = 10;
            Defence = 5;
            Gold = 30;
            Description = "Walrus";
            Inventory.Add( "Ivory Tusk" );
        }

    }
}
