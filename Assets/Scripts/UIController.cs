﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TextRPG {

    public class UIController : MonoBehaviour {

        [SerializeField] Text PlayerStatsText, EnemyStatsText, PlayerInventoryText;
        [SerializeField] Player player;

        public delegate void OnPlayerUpdateHandler();
        public static OnPlayerUpdateHandler OnPlayerStatChange;
        public static OnPlayerUpdateHandler OnPlayerInventoryChange;

        public delegate void OnEnemyUpdateHandler(Enemy enemy);
        public static OnEnemyUpdateHandler OnEnemyUpdate;

        private void Start() {

            OnPlayerStatChange += UpdatePlayerStats;
            OnPlayerInventoryChange += UpdatePlayerInventory;
            OnEnemyUpdate += UpdateEnemyStats;
        }

        public void UpdatePlayerStats() {

            PlayerStatsText.text = string.Format(
                "Player: {0} energy, {1} attack, {2} defence, {3} gold",
                player.Energy,
                player.Attack,
                player.Defence,
                player.Gold
                );
        }

        public void UpdatePlayerInventory() {

            PlayerInventoryText.text = "Inventory: ";

            foreach(string item in player.Inventory ) {
                PlayerInventoryText.text += item + " / ";
            }
        }

        public void UpdateEnemyStats(Enemy enemy) {

            if( enemy ) {
                EnemyStatsText.text = string.Format(
                    "{0}: {1} energy, {2} attack, {3} defence",
                    enemy.Description,
                    enemy.Energy,
                    enemy.Attack,
                    enemy.Defence
                );
            } else {
                EnemyStatsText.text = "";
            }
        }
    }
}