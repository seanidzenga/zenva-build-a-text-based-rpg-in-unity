﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TextRPG {

    public class ItemDatabase : MonoBehaviour {

        public static ItemDatabase Instance { get; private set; }
        public List<string> Items { get; set; } = new List<string>();

        private void Awake() {

            if(Instance != null && Instance != this ) {
                Destroy( this.gameObject );
            } else {
                Instance = this;
            }

            Items.Add( "Emerald Slipper" );
            Items.Add( "Empty Chalice" );
            Items.Add( "Bowtie" );
            Items.Add( "Ivory Tusk" );
            Items.Add( "Bandit Mask" );
        }
    }
}

